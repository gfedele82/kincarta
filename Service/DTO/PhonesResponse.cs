﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class PhonesResponse
    {
        public virtual int Id { get; set; }
        public virtual string Number { get; set; }
        public virtual string Type { get; set; }
    }
}
