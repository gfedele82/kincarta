﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class StatesResponse
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
