﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public class ContractsResponse
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Company { get; set; }
        public virtual string Image { get; set; }
        public virtual string Email { get; set; }
        public virtual string BirthDate { get; set; }
        public virtual string Address { get; set; }
        public virtual CitiesResponse City { get; set; }
        public virtual List<PhonesResponse> Phone { get; set; }
    }
}
