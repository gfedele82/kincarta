﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CitiesUpdateRequest
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual StatesUpdateRequest State { get; set; }
    }
}
