﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class PhonesCreateRequest
    {
        public virtual string Number { get; set; }
        public virtual int Type { get; set; }
    }
}
