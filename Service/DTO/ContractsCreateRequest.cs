﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ContractsCreateRequest
    {
        public virtual string Name { get; set; }
        public virtual string Company { get; set; }
        public virtual string Image { get; set; }
        public virtual string Email { get; set; }
        public virtual DateTime BirthDate { get; set; }
        public virtual string Address { get; set; }
        public virtual CitiesCreateRequest City { get; set; }
        public virtual List<PhonesCreateRequest> Phone { get; set; }
    }
}
