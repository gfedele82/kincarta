﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CitiesResponse
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual StatesResponse State { get; set; }
    }
}
