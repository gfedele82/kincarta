﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class BadRequest
    {
        public BadRequest()
        {
            this.error = new List<string>();

        }
        public List<string> error { get; set; }
    }
}
