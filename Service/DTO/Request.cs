﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class Request
    {
        public Request()
        {
            this.Msg = new List<string>();

        }
        public List<string> Msg { get; set; }
    }
}
