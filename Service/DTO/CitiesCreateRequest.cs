﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class CitiesCreateRequest
    {
        public virtual string Name { get; set; }
        public virtual StatesCreateRequest State { get; set; }
    }
}
