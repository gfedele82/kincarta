﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class StatesUpdateRequest
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
