﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Cfg;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Entities;
using Microsoft.Extensions.Configuration;
using NHibernate.Tool.hbm2ddl;

namespace Service.Ado
{
    public static class FluentNHibernateHelper
    {

        //var listOfEntityMap = typeof(M).Assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(M))).ToList();  
        //var sessionFactory = SessionFactoryBuilder.BuildSessionFactory(dbmsTypeAsString, connectionStringName, listOfEntityMap, withLog, create, update);  
        public static ISessionFactory BuildSessionFactory(bool create = false, bool update = false)
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("appsettings.json")
                .Build();

            string _connstring = configuration.GetConnectionString("default");

            return Fluently.Configure().Database(MsSqlConfiguration.MsSql2012.ConnectionString(_connstring))
                //.Mappings(m => entityMappingTypes.ForEach(e => { m.FluentMappings.Add(e); }))  
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<NHibernate.Cfg.Mappings>()).CurrentSessionContext("call").ExposeConfiguration(cfg => BuildSchema(cfg, create, update)).BuildSessionFactory();
        }
        /// <summary>  
        /// Build the schema of the database.  
        /// </summary>  
        /// <param name="config">Configuration.</param>  
        private static void BuildSchema(Configuration config, bool create = false, bool update = false)
        {
            if (create)
            {
                new SchemaExport(config).Create(false, true);
            }
            else
            {
                new SchemaUpdate(config).Execute(false, update);
            }
        }

        //public static ISession OpenSession()
        //{

        //    string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
        //    IConfigurationRoot configuration = new ConfigurationBuilder()
        //        .SetBasePath(projectPath)
        //        .AddJsonFile("appsettings.json")
        //        .Build();

        //    string _connstring = configuration.GetConnectionString("default");

        //    ISessionFactory sesionFactory = Fluently.Configure().Database(MsSqlConfiguration.MsSql2012.ConnectionString(_connstring).ShowSql()).Mappings(
        //       m => m.FluentMappings.AddFromAssemblyOf<Contracts>()).ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false)).BuildSessionFactory();
        //    return sesionFactory.OpenSession();
        //}

        //private const string CurrentSessionKey = "nhibernate.current_session";
        //private static readonly ISessionFactory _sessionFactory;
        //static FluentNHibernateHelper()
        //{
        //    _sessionFactory = FluentConfigure();
        //}
        //public static ISession GetCurrentSession()
        //{
        //    return _sessionFactory.OpenSession();
        //}
        //public static void CloseSession()
        //{
        //    _sessionFactory.Close();
        //}
        //public static void CloseSessionFactory()
        //{
        //    if (_sessionFactory != null)
        //    {
        //        _sessionFactory.Close();
        //    }
        //}

        //public static ISessionFactory FluentConfigure()
        //{
        //    string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
        //    IConfigurationRoot configuration = new ConfigurationBuilder()
        //        .SetBasePath(projectPath)
        //        .AddJsonFile("appsettings.json")
        //        .Build();

        //    string _connstring = configuration.GetConnectionString("default");

        //    return Fluently.Configure()
        //        //which database
        //        .Database(
        //            MsSqlConfiguration.MsSql2012
        //                .ConnectionString(_connstring) //connection string from app.config
        //                                            .ShowSql()
        //                )
        //        //2nd level cache
        //        //.Cache(
        //        //    c => c.UseQueryCache()
        //        //        .UseSecondLevelCache()
        //        //        .ProviderClass<NHibernate.Cache.HashtableCacheProvider>())
        //        //find/set the mappings
        //        //.Mappings(m => m.FluentMappings.AddFromAssemblyOf<Contracts>())
        //        //.Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
        //    //        .BuildSessionFactory();
        //}
    }
}
