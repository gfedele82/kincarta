﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Service.Ado
{
    public class PhonesAd : Helper
    {
        public long Create(Phones _pho)
        {
            long resp = base.Create<Phones>(_pho);

            return resp;
        }

        public bool Update(Phones _pho)
        {
            bool resp = base.Update<Phones>(_pho);

            return resp;

        }

        public bool Delete(Phones _pho)
        {
            bool resp = base.Delete<Phones>(_pho);

            return resp;

        }

        public Phones ListByPhoneId(int _idpho)
        {
            Conn();

            var _query = $"select * from dbo.Phones where Id = {_idpho.ToString()}";

            var resp = _conn.Query<Phones>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (Phones)resp.ToList()[0];
            else
                return null;
        }
    }
}
