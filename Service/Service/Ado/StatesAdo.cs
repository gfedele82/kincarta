﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Dapper.Contrib.Extensions;
using Dapper;

namespace Service.Ado
{
    public class StatesAdo : Helper
    {
        public List<States> ListAll()
        {
            return (List<States>)base.ListAll<States>();
        }
        public long Create(States _ste)
        {
            long resp = base.Create<States>(_ste);

            return resp;

        }

        public States ListByStateId(int _idste)
        {
            Conn();

            var _query = $"select * from dbo.States where Id = {_idste.ToString()}";

            var resp = _conn.Query<States>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (States)resp.ToList()[0];
            else
                return null;
        }

        public States ListByStateName(string _nameste)
        {
            Conn();

            var _query = $"select * from dbo.States where Name like '%{_nameste}%'";

            var resp = _conn.Query<States>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (States)resp.ToList()[0];
            else
                return null;
        }
    }
}
