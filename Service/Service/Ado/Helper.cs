﻿using System;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Reflection;

namespace Service.Ado
{
    public abstract class Helper
    {
        string _connstring;
        protected SqlConnection _conn;
        public Helper()
        {
            string projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new String[] { @"bin\" }, StringSplitOptions.None)[0];
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("appsettings.json")
                .Build();

            _connstring = configuration.GetConnectionString("default");
        }

        protected void Conn()
        {
            _conn = new SqlConnection(_connstring);
            _conn.Open();
        }

        protected IList<T> ListAll<T>()
        {
            IList<T> _list = new List<T>();

            Conn();

            string _query = $"SELECT * FROM dbo.{ typeof(T).Name.ToString()}";

            var resp = _conn.Query<T>(_query);

            _conn.Close();

            return (List<T>)resp;
        }

        protected long Create<T>(object _obj) where T : class
        {

            Conn();

            long resp = _conn.Insert<T>((T)_obj);

            _conn.Close();

            return resp;
        }

        protected bool Update<T>(object _obj) where T : class
        {

            Conn();

            bool resp = _conn.Update<T>((T)_obj);

            _conn.Close();

            return resp;
        }

        protected bool Delete<T>(object _obj) where T : class
        {

            Conn();

            bool resp = _conn.Delete<T>((T)_obj);

            _conn.Close();

            return resp;
        }
    }
}
