﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Dapper.Contrib.Extensions;
using Dapper;

namespace Service.Ado
{
    public class CitiesAdo :Helper
    {
        public long Create(Cities _cit)
        {
            long resp = base.Create<Cities>(_cit);

            return resp;

        }

        public Cities ListByCityId(int _idcit)
        {
            Conn();

            var _query = $"select * from dbo.Cities where Id = {_idcit.ToString()}";

            var resp = _conn.Query<Cities>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (Cities)resp.ToList()[0];
            else
                return null;
        }

        public Cities ListByCityName_StateId(int _idste, string _namecit)
        {
            Conn();

            var _query = $"select * from dbo.Cities where IdState = {_idste.ToString()} and Name Like '%{_namecit}%'";

            var resp = _conn.Query<Cities>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (Cities)resp.ToList()[0];
            else
                return null;
        }
    }
}
