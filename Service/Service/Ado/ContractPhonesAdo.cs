﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Service.Ado
{
    public class ContractPhonesAdo : Helper
    {
        public List<Phones> ListByContractId(int _idcontr)
        {
            Conn();

            var _query = $"select a.Id, a.Number, a.Type from dbo.Phones as a, dbo.ContractPhones as b where a.Id = b.IdPhone and b.IdContract = {_idcontr.ToString()}";

            var resp = _conn.Query<Phones>(_query);

            _conn.Close();

            return resp.ToList();
        }

        public long Create( ContractPhones _contpho)
        {
            long resp = base.Create<ContractPhones>(_contpho);

            return resp;

        }

        public int Delete(ContractPhones _contpho)
        {
            Conn();

            string query = $"delete from dbo.ContractPhones where IdPhone = {_contpho.IdPhone.ToString() } and IdContract = { _contpho.IdContract.ToString()}";

            int resp = _conn.Execute(query);

            _conn.Close();
            return resp;

        }
    }
}
