﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Service.Ado
{
    public class ContractsAdo : Helper
    {

        public  List<Contracts> ListAll()
        {
             return (List<Contracts>)base.ListAll<Contracts>();
        }

        public Contracts ListByContractId(int _id)
        {
            Conn();

            var _query = $"select * from dbo.Contracts where Id = {_id.ToString()}";

            var resp = _conn.Query<Contracts>(_query);

            _conn.Close();

            if (resp.ToList().Count > 0)
                return (Contracts)resp.ToList()[0];
            else
                return null;
        }

        public long Create(Contracts _cont)
        {

            long resp = base.Create<Contracts>(_cont);

            return resp;

        }

        public bool Update(Contracts _cont)
        {

            bool resp = base.Update<Contracts>(_cont);

            return resp;

        }

        public bool Delete(Contracts _cont)
        {

            bool resp = base.Delete<Contracts>(_cont);

            return resp;

        }
    }
}
