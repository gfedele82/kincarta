﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities;
using DTO;

namespace Service.Mapper
{
    public class CitiesProfile : Profile
    {
        public CitiesProfile()
        {
            CreateMap<Cities, CitiesResponse>().ReverseMap();
            CreateMap<CitiesCreateRequest, Cities > ().ReverseMap();
            CreateMap<CitiesUpdateRequest, Cities>().ReverseMap();
        }
    }
}
