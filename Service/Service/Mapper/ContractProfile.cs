﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities;
using DTO;

namespace Service.Mapper
{
    public class ContractProfile : Profile
    {
        public ContractProfile() 
        {
            CreateMap<Contracts, ContractsResponse>()
            .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(src => src.BirthDate.ToString("dd-MM-yyyy")));
            CreateMap<ContractsCreateRequest, Contracts>().ReverseMap();
            CreateMap<ContractsUpdateRequest, Contracts>().ReverseMap();


        }
    }
}
