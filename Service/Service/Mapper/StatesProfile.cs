﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities;
using DTO;


namespace Service.Mapper
{
    public class StatesProfile : Profile
    {
        public StatesProfile()
        {
            CreateMap<States, StatesResponse>().ReverseMap();
            CreateMap<StatesCreateRequest, States>().ReverseMap();
            CreateMap<StatesUpdateRequest, States>().ReverseMap();
        }
    }
}
