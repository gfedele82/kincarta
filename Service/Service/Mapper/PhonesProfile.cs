﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities;
using DTO;

namespace Service.Mapper
{
    public class PhonesProfile : Profile
    {
        public PhonesProfile()
        {
            CreateMap<Phones, PhonesResponse>().ReverseMap();
            CreateMap<PhonesCreateRequest, Phones>().ReverseMap();
            CreateMap<PhonesUpdateRequest, Phones>().ReverseMap();
        }
    }
}
