﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using DTO;
using AutoMapper;

namespace Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContractController : ControllerBase
    {
        private readonly ILogger<ContractController> _logger;
        private readonly IMapper _mapper;

        public ContractController(ILogger<ContractController> logger, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            try
            {
                Business.ContractBN _nbcontact = new Business.ContractBN();


                var _list = _mapper.Map<IEnumerable<ContractsResponse>>(_nbcontact.ListAll());
                return Ok(_list);
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }
        }

        [HttpGet]
        [Route("GetById")]
        public IActionResult Get(int id)
        {
            try
            {
                if (id < 1)
                {
                    BadRequest _bad = new BadRequest();
                    _bad.error.Add("The id has to be greater than 0");
                    return Ok(_bad);
                }

                Business.ContractBN _nbcontact = new Business.ContractBN();

                Contracts _cont = _nbcontact.ListById(id);
                if (_cont == null)
                {
                    Request _req = new Request();
                    _req.Msg.Add($"Id: {id} not found");
                    return Ok(_req);

                }
                else
                {
                    ContractsResponse __cont = _mapper.Map<ContractsResponse>(_cont);

                    return Ok(__cont);
                }
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }

        }

        [HttpGet]
        [Route("GetFilter")]
        public IActionResult GetFilter(string city ="", string state="")
        {
            try
           {
                Business.ContractBN _nbcontact = new Business.ContractBN();

                var _list = _mapper.Map<IEnumerable<ContractsResponse>>(_nbcontact.ListFilter(city, state));

                return Ok(_list);
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }

}

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(ContractsCreateRequest _cont)
        {
            try
            {
                BadRequest _bad = Validate.Validate.validater(_cont);

                if(_bad.error.Count > 0)
                
                {
                    return Ok(_bad.error);
                }

                Business.ContractBN _nbcontact = new Business.ContractBN();

                Contracts __cont = _mapper.Map<Contracts>(_cont);

                if(_nbcontact.Create(__cont))
                {
                    return Ok(true);
                }
                else
                {
                    _bad = new BadRequest();
                    _bad.error.Add("Problems performing the action");
                    return Ok(_bad);
                }
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }

}

        [HttpPost]
        [Route("Update")]
        public IActionResult Update(ContractsUpdateRequest _cont)
        {
            try
            {
                BadRequest _bad = Validate.Validate.validater(_cont);

                if (_bad.error.Count > 0)

                {
                    return Ok(_bad.error);
                }

                Business.ContractBN _nbcontact = new Business.ContractBN();

                Contracts __cont = _mapper.Map<Contracts>(_cont);

                if (_nbcontact.ListById(__cont.Id) == null)
                {
                    _bad = new BadRequest();
                    _bad.error.Add("The contract doesn't exist");
                    return Ok(_bad);
                }


                if (_nbcontact.Update(__cont))
                {
                    return Ok(true);
                }
                else
                {
                    _bad = new BadRequest();
                    _bad.error.Add("Problems performing the action");
                    return Ok(_bad);
                }
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public IActionResult Delete(int id)
        {
            try
            {
                BadRequest _bad = new BadRequest();
                if (id < 1)
                {
                    _bad = new BadRequest();
                    _bad.error.Add("The id has to be greater than 0");
                    return Ok(_bad);
                }

                Business.ContractBN _nbcontact = new Business.ContractBN();

                if (_nbcontact.ListById(id) == null)
                {
                    _bad = new BadRequest();
                    _bad.error.Add("The contract doesn't exist");
                    return Ok(_bad);
                }

                if (_nbcontact.Delete(id))
                {
                    return Ok(true);
                }
                else
                {
                    _bad = new BadRequest();
                    _bad.error.Add("Problems performing the action");
                    return Ok(_bad);
                }
            }
            catch (Exception ex)
            {
                string resp = $"Error: { ex.Message}";
                return NotFound(resp);
            }
        }
    }
}
