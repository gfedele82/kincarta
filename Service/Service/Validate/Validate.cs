﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using DTO;
using System.Text.RegularExpressions;

namespace Service.Validate
{
    public static class Validate
    {
        public static BadRequest validater(ContractsCreateRequest _cont)
        {
            BadRequest _bad = new BadRequest();

            if (_cont.Name.Length == 0)
            {
                _bad.error.Add("Name is required");
            }
            if (_cont.Company.Length == 0)
            {
                _bad.error.Add("Company is required");
            }
            if (_cont.Image.Length == 0)
            {
                _bad.error.Add("Image is required");
            }
            else
            {
                string _ext = System.IO.Path.GetExtension(_cont.Image);
                if (_ext.ToUpper() != ".jpe"&&  _ext != ".jpg" && _ext != ".png")
                    _bad.error.Add("Image extension is mut be jpe, jpg, png");

            }
            if (_cont.Email.Length == 0)
            {
                _bad.error.Add("Email is required");
            }
            else
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(_cont.Email);
                if (!match.Success)
                    _bad.error.Add("Email's format is incorect");

            }
            if (_cont.BirthDate.Date >= DateTime.Now.Date )
            {
                _bad.error.Add("BirthDate cannot be higher than the current day");
            }
            if (_cont.Address.Length == 0)
            {
                _bad.error.Add("Address is required");
            }
            if (_cont.City.Name.Length == 0)
            {
                _bad.error.Add("City is required");
            }
            if (_cont.City.State.Name.Length == 0)
            {
                _bad.error.Add("State is required");
            }
            foreach (PhonesCreateRequest _pho in _cont.Phone)
            { 
                if(_pho.Type != 0 && _pho.Type != 1)
                {
                    _bad.error.Add("Phone type are 0 = work and 1 = personal");
                    break;
                }
                if (_pho.Number.Length == 0)
                {
                    _bad.error.Add("Phone number is required");
                    break;
                }
                else
                {
                    if(!_pho.Number.All(char.IsDigit))
                    {
                        _bad.error.Add("Phone number is must to be integer");
                        break;
                    }
                    else if(int.Parse(_pho.Number) < 0 || _pho.Number.Length != 8)
                    {
                        _bad.error.Add("Phone number is must to be positive and has 8 digit");
                        break;
                    }

                }

            }
            return _bad;

        }

        public static BadRequest validater(ContractsUpdateRequest _cont)
        {
            BadRequest _bad = new BadRequest();
            if (_cont.Id < 0)
            {
                _bad.error.Add("Id is required");
            }
            if (_cont.Name.Length == 0)
            {
                _bad.error.Add("Name is must to be positive");
            }
            if (_cont.Company.Length == 0)
            {
                _bad.error.Add("Company is required");
            }
            if (_cont.Image.Length == 0)
            {
                _bad.error.Add("Image is required");
            }
            else
            {
                string _ext = System.IO.Path.GetExtension(_cont.Image);
                if (_ext.ToUpper() != ".jpe" && _ext != ".jpg" && _ext != ".png")
                    _bad.error.Add("Image extension is mut be jpe, jpg, png");

            }
            if (_cont.Email.Length == 0)
            {
                _bad.error.Add("Email is required");
            }
            else
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(_cont.Email);
                if (!match.Success)
                    _bad.error.Add("Email's format is incorect");

            }
            if (_cont.BirthDate.Date >= DateTime.Now.Date)
            {
                _bad.error.Add("BirthDate cannot be higher than the current day");
            }
            if (_cont.Address.Length == 0)
            {
                _bad.error.Add("Address is required");
            }
            if (_cont.City.Name.Length == 0)
            {
                _bad.error.Add("City is required");
            }
            if (_cont.City.State.Name.Length == 0)
            {
                _bad.error.Add("State is required");
            }
            foreach (PhonesUpdateRequest _pho in _cont.Phone)
            {
                if (_pho.Id < 0)
                {
                    _bad.error.Add("Phone id is must to be positive");
                    break;
                }
                if (_pho.Type != 0 && _pho.Type != 1)
                {
                    _bad.error.Add("Phone type are 0 = work and 1 = personal");
                    break;
                }
                if (_pho.Number.Length == 0)
                {
                    _bad.error.Add("Phone number is required");
                    break;
                }
                else
                {
                    if (!_pho.Number.All(char.IsDigit))
                    {
                        _bad.error.Add("Phone number is must to be integer");
                        break;
                    }
                    else if (int.Parse(_pho.Number) < 0 || _pho.Number.Length != 8)
                    {
                        _bad.error.Add("Phone number is must to be positive and has 8 digit");
                        break;
                    }

                }

            }
            return _bad;

        }
    }
}
