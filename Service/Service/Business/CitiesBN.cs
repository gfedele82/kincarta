﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;

namespace Service.Business
{
    public class CitiesBN
    {
        public Cities ListById(int _id)
        {
            Ado.CitiesAdo _citado = new Ado.CitiesAdo();
            Ado.StatesAdo _steado = new Ado.StatesAdo();

            Cities _cit = _citado.ListByCityId(_id);

            if (_cit != null)
                _cit.State = _steado.ListByStateId(_cit.State.Id);

            return _cit;

        }

        public bool Create(Cities _cit)
        {
            Ado.CitiesAdo _citado = new Ado.CitiesAdo();
            Ado.StatesAdo _steado = new Ado.StatesAdo();

            States _ste = _steado.ListAll().Where(X => X.Name.ToUpper().Equals(_cit.State.Name.ToUpper())).FirstOrDefault();

            return true;
        }
    }
}
