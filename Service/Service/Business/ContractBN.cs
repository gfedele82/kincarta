﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;



namespace Service.Business
{
    public class ContractBN
    {

        public List<Contracts> ListAll()
        {
            Ado.ContractsAdo _conado = new Ado.ContractsAdo();
            Ado.ContractPhonesAdo _phoado = new Ado.ContractPhonesAdo();
            Business.CitiesBN _bncit = new Business.CitiesBN();

            List<Contracts> _lcont = _conado.ListAll();

            _lcont.ForEach(X => X.Phone = _phoado.ListByContractId(X.Id));

            _lcont.ForEach(X => X.City = _bncit.ListById(X.City.Id));

            return _lcont;

        }

        public List<Contracts> ListFilter(string city = "", string state = "")
        {
            List<Contracts> _lcon = new List<Contracts>();

            _lcon = this.ListAll();

            if (city.Length != 0)
                _lcon = _lcon.Where(X => X.City.Name.ToUpper() == city.ToUpper()).ToList();

            if (state.Length != 0)
                _lcon = _lcon.Where(X => X.City.State.Name.ToUpper() == state.ToUpper()).ToList();

            return _lcon;
        }


        public Contracts ListById(int _id)
        {
            Ado.ContractsAdo _conado = new Ado.ContractsAdo();
            Ado.ContractPhonesAdo _phoado = new Ado.ContractPhonesAdo();
            Business.CitiesBN _bncit = new Business.CitiesBN();

            Contracts _cont = _conado.ListByContractId(_id);

            if (_cont != null)
            {
                _cont.Phone = _phoado.ListByContractId(_cont.Id);
                _cont.City = _bncit.ListById(_cont.City.Id);
            }

                return _cont;

        }

        public bool Create (Contracts _cont)
        {
            Ado.StatesAdo _adoste = new Ado.StatesAdo();
            Ado.CitiesAdo _adocit = new Ado.CitiesAdo();

            //me fijo el estado
            States _sta = _adoste.ListByStateName(_cont.City.State.Name);
            if (_sta == null)
            {
                //creo el estado
                _sta = new States()
                { Name = _cont.City.State.Name };
                _sta.Id = (int)_adoste.Create(_sta);
            }
            //creo la ciudad
            Cities _cit = _adocit.ListByCityName_StateId(_sta.Id, _cont.City.Name);
            if (_cit == null)
            {
                _cit = new Cities()
                {
                    Name = _cont.City.Name,
                    IdState = _sta.Id
                };
                _cit.Id = (int)_adocit.Create(_cit);
            }

            _cont.City = _cit;

            long _id = new Ado.ContractsAdo().Create(_cont);


            foreach (Phones _pho in _cont.Phone)
            {
                ContractPhones _conpho = new ContractPhones();
                _conpho.IdContract = (int)_id;
                _conpho.IdPhone = (int)new Ado.PhonesAd().Create(_pho);
                new Ado.ContractPhonesAdo().Create(_conpho);
            }

            return true;
        }

        public bool Update(Contracts _cont)
        {
            Ado.ContractsAdo _adocont = new  Ado.ContractsAdo();
            Ado.PhonesAd _adopho = new Ado.PhonesAd();
            Ado.ContractPhonesAdo _adocontpho = new Ado.ContractPhonesAdo();
            Ado.StatesAdo _adoste = new Ado.StatesAdo();
            Ado.CitiesAdo _adocit = new Ado.CitiesAdo();

            //me fijo el estado
            States _sta = _adoste.ListByStateName(_cont.City.State.Name);
            if (_sta == null)
            {
                //creo el estado
                _sta = new States()
                { Name = _cont.City.State.Name };
                _sta.Id = (int)_adoste.Create(_sta);
            }
            //creo la ciudad
            Cities _cit = _adocit.ListByCityName_StateId(_sta.Id, _cont.City.Name);
            if (_cit == null)
            {
                _cit = new Cities()
                {
                    Name = _cont.City.Name,
                    IdState = _sta.Id
                };
                _cit.Id = (int)_adocit.Create(_cit);
            }

            _cont.City = _cit;

            if (_adocont.ListByContractId(_cont.Id) != null)
            {
                //bool _id = _adocont.Update(_cont);
                foreach (Phones _pho in _cont.Phone)
                {
                    if (_adopho.ListByPhoneId(_pho.Id) == null)
                    {
                        //la creo
                        ContractPhones _contpho = new ContractPhones();
                        _contpho.IdContract = _cont.Id;
                        _contpho.IdPhone = (int)_adopho.Create(_pho);
                        _pho.Id = _contpho.IdPhone;
                        _adocontpho.Create(_contpho);
                    }
                    else
                    {
                        //actualizo
                        _adopho.Update(_pho);
                    }
                }
                //borro los que no viniero
                List<Phones> _delphone = _adocontpho.ListByContractId(_cont.Id).Where(x => !_cont.Phone.Any(y => y.Id == x.Id)).ToList();
                foreach (Phones _pho in _delphone)
                {
                    //la borro
                    ContractPhones _contpho = new ContractPhones();
                    _contpho.IdContract = _cont.Id;
                    _contpho.IdPhone = _pho.Id;

                    _adocontpho.Delete(_contpho);
                    _adopho.Delete(_pho);

                }

                _adocont.Update(_cont);

            }
            else
                return false;

            return true;
        }

        public bool Delete(int _id)
        {
            Ado.ContractsAdo _adocont = new Ado.ContractsAdo();
            Ado.PhonesAd _adopho = new Ado.PhonesAd();
            Ado.ContractPhonesAdo _adocontpho = new Ado.ContractPhonesAdo();


            Contracts _cont = ListById(_id);
            if (_cont != null)
            {
                foreach(Phones _pho in _cont.Phone)
                {
                    ContractPhones _contpho = new ContractPhones()
                    { IdContract = _cont.Id,
                     IdPhone = _pho.Id
                    };

                    _adocontpho.Delete(_contpho);
                    _adopho.Delete(_pho);
                }
                _adocont.Delete(_cont);
            }
            else
                return false;

            return true;
        }
    }
}
