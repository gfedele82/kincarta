﻿CREATE TABLE [dbo].[Cities] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [Name]    VARCHAR (50) NOT NULL,
    [IdState] INT          NOT NULL,
    CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Cities_States] FOREIGN KEY ([IdState]) REFERENCES [dbo].[States] ([Id])
);

