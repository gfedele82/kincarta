﻿CREATE TABLE [dbo].[PhoneTypes] (
    [Id]   INT          NOT NULL,
    [Name] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_PhoneTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

