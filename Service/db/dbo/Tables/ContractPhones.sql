﻿CREATE TABLE [dbo].[ContractPhones] (
    [IdContract] INT NOT NULL,
    [IdPhone]    INT NOT NULL,
    CONSTRAINT [PK_ContractPhones] PRIMARY KEY CLUSTERED ([IdContract] ASC, [IdPhone] ASC),
    CONSTRAINT [FK_ContractPhones_Contracts] FOREIGN KEY ([IdContract]) REFERENCES [dbo].[Contracts] ([Id]),
    CONSTRAINT [FK_ContractPhones_Phones] FOREIGN KEY ([IdPhone]) REFERENCES [dbo].[Phones] ([Id])
);