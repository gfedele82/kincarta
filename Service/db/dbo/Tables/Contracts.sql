﻿CREATE TABLE [dbo].[Contracts] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]      VARCHAR (50)  NOT NULL,
    [Company]   VARCHAR (50)  NOT NULL,
    [Image]     VARCHAR (250) NOT NULL,
    [Email]     VARCHAR (100) NOT NULL,
    [BirthDate] DATE          NOT NULL,
    [Address]   VARCHAR (250) NOT NULL,
    [IdCity]    INT           NOT NULL,
    CONSTRAINT [PK_Contracts] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Contracts_Cities] FOREIGN KEY ([IdCity]) REFERENCES [dbo].[Cities] ([Id])
);





