﻿CREATE TABLE [dbo].[Phones] (
    [Id]     INT            IDENTITY (1, 1) NOT NULL,
    [Number] varchar (50) NOT NULL,
    [Type]   INT            NOT NULL,
    CONSTRAINT [PK_Phones] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Phones_PhoneTypes] FOREIGN KEY ([Type]) REFERENCES [dbo].[PhoneTypes] ([Id])
);

