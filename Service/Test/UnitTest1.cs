using System;
using Xunit;
using DTO;
using Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using FluentAssertions;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;

namespace Test
{
    public class UnitTest1
    {
        private   HttpClient _client { get; }

        public UnitTest1(WebApplicationFactory<Service.Startup> fixture)
        {
            var server = new TestServer(new WebHostBuilder().UseEnvironment("Deleveloper").UseStartup<Service.Startup>());
            _client = server.CreateClient();
        }

        [Theory]
        [InlineData("GET")]
        public async Task Test1(string method)
        {
            var request = new HttpRequestMessage(new HttpMethod(method), "/Contract/GetAll");
            var response = await _client.SendAsync(request);

            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
