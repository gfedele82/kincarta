﻿using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;

namespace Entities
{
    [Table("dbo.Contracts")]
    public class Contracts
    {
        public Contracts()
        {
            if (this.City == null)
                this.City = new Cities();
        }

        [Computed]
        public virtual int Id { get; set; }

        [Computed]
        public virtual List<Phones> Phone { get; set; }

        [Computed]
        public virtual Cities City { get; set; }

        public virtual int IdCity
        {
            get
            {
                return this.City.Id;
            }
            set
            {
                this.City.Id = value;
            }
        }

        public virtual string Name { get; set; }

        public virtual string Company { get; set; }

        public virtual string Image { get; set; }

        public virtual string Email { get; set; }

        public virtual DateTime BirthDate { get; set; }

        public virtual string Address { get; set; }

    }
}
