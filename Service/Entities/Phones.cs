﻿using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using System.Text;

namespace Entities
{
    [Table("dbo.Phones")]
    public class Phones
    {
        [Computed]
        public virtual int Id { get; set; }
        public virtual string Number { get; set; }
        public virtual TypePhone Type { get; set; }
    }

}
