﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
    [Table("dbo.ContractPhones")]
    public class ContractPhones
    {
        public virtual int IdContract { get; set; }
        public virtual int IdPhone { get; set; }
    }
}
