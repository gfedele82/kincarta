﻿using System;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using System.ComponentModel.Design;
using System.Text;

namespace Entities
{
    [Table("dbo.Cities")]
    public class Cities 
    {
        public Cities()
        {
            if (this.State == null)
                this.State = new States();

        }

        [Computed]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        [Computed]
        public virtual States State { get; set; }

        public virtual int IdState {
            get { return this.State.Id; }
            set { this.State.Id = value; }
        }
    }
}
