﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper.Contrib.Extensions;

namespace Entities
{
    [Table("dbo.States")]
    public class States
    {
        [Computed]
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
